<!DOCTYPE html>
<html>
<head>
	<title>Data Perusahaan</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body>
    <div class="container">
		<div class="card">
			<div class="card-body">
 
                <h2 style="text-align:center;">TAMBAH DATA PERUSAHAAN</h2>
                <hr></hr>
                <br/>

                <a class="btn btn-success" href="index.php">KEMBALI</a>
                <br/>
                <br/>
                
                <form method="post" action="aksi_tambah.php">
                    <table class="table table-bordered">
                        <tr>			
                            <td>Nama</td>
                            <td><input class="form-control" type="text" name="nama"></td>
                        </tr>
                        <tr>
                            <td>DESKRIPSI</td>
                            <td><textarea class="form-control" type="text" name="deskripsi"></textarea></td>
                        </tr>
                        <tr>
                            <td>AKSI</td>
                            <td><input class="btn btn-primary" type="submit" value="SIMPAN"></td>
                        </tr>		
                    </table>
                </form>
            </div>
        </div>
    </div>
</body>
</html>