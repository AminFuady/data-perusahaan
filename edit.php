<!DOCTYPE html>
<html>
<head>
	<title>Data Perusahaan</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body>
    <div class="container"> 
		<div class="card">
			<div class="card-body">
 
                <h2 style="text-align:center;">EDIT DATA PERUSAHAAN</h2>
                <hr></hr>
                <br/>

                <a class="btn btn-success" href="index.php">KEMBALI</a>
                <br/>
                <br/>
            
                <?php
                include 'koneksi.php';
                $id = $_GET['id'];
                $data = mysqli_query($koneksi,"select * from perusahaan where id='$id'");
                while($d = mysqli_fetch_array($data)){
                    ?>
                    <form method="post" action="update.php">
                        <table class="table table-bordered">
                            <tr>			
                                <td>Nama</td>
                                <td>
                                    <input type="hidden" name="id" value="<?php echo $d['id']; ?>">
                                    <input class="form-control" type="text" name="nama" value="<?php echo $d['nama']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td>
                                <td><textarea class="form-control" type="text" name="deskripsi"><?php echo $d['deskripsi']; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>AKSI</td>
                                <td><input class="btn btn-primary" type="submit" value="SIMPAN"></td>
                            </tr>		
                        </table>
                    </form>
                    <?php 
                }
                ?>
            </div>
        </div>
    </div>
</body>
</html>