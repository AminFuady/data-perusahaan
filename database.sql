-- Adminer 4.6.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `perusahaan`;
CREATE TABLE `perusahaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `perusahaan` (`id`, `nama`, `deskripsi`) VALUES
(1,	'PT Indofood Sukses Makmur Tbk',	'Merupakan produsen segala jenis makanan dan juga minuman. Markas perusahaan ini ada di Jakarta, Indonesia. Pertama kali didirikan pada 14 Agustus 1990 oleh Sudono Salim.'),
(2,	'PT Bank Central Asia Tbk',	'Merupakan bank swasta terbesar di Indonesia yang didirikan pada 21 Februari 1957 dan pernah menjadi bagian dari perusahaan Salim Group. BCA pernah menjadi bank negara akibat krisis tahun 1998 namun pada tahun 2002, BPPN melepas 51% saham di BCA melalui Farindo Investment, Ltd, yang berbasis di Mauritus.'),
(3,	'PT Telekomunikasi Indonesia Tbk (Persero)',	'Perusahaan informasi dan komunikasi serta sebagai penyedia jasa dan jaringan telekomunikasi lengkap di Indonesia.');

-- 2019-10-15 04:45:56
