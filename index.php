<!DOCTYPE html>
<html>
<head>
	<title>Data Perusahaan</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body>
    <div class="container">
		<div class="card">
			<div class="card-body">

                <h2 style="text-align:center;">DATA PERUSAHAAN</h2>
                <hr></hr>
                <br/>

                <a class="btn btn-primary" href="tambah.php">+ TAMBAH DATA</a>
                <br/>
                <br/>

                <table class="table table-bordered">
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                    </tr>
                    <?php
                        include 'koneksi.php';
                        $no = 1;
                        $data = mysqli_query($koneksi,"select * from perusahaan");
                        while($d = mysqli_fetch_array($data)){
                    ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $d['nama']; ?></td>
                            <td><?php echo $d['deskripsi']; ?></td>
                            <td>
                                <a class="btn btn-warning btn-sm" href="edit.php?id=<?php echo $d['id']; ?>">EDIT</a>
                                <a class="btn btn-danger btn-sm" href="hapus.php?id=<?php echo $d['id']; ?>">HAPUS</a>
                            </td>
                        </tr>
                        <?php 
                        }
                        ?>
                </table>
            </div>
        </div>
    </div>

</body>
</html>